import 'dart:convert';
import 'dart:io';
import 'package:test/expect.dart';
import 'package:test/test.dart';
import '../bin/repasto.dart';

const prefix = '/project/catch/';

const rawJson = '''
{"Parameters": [
{"Name": "/lost/in/translation", "Value": "do-not-catch-me"},
{"Name": "/project/catch/sname", "Value": "expect-me"},
{"Name": "/project/catch/document/root", "Value": "the_road_is_long"},
{"Name": "/project/catch/name-with-dashes", "Value": "them-dashes"}
]}
''';
const rawJsonRootValue = '''
{"Parameters": [
{"Name": "/lost/in/translation", "Value": "do-not-catch-me"},
{"Name": "/project/catch/sname", "Value": "expect-me"},
{"Name": "/project/catch/document/root/data", "Value": "deep"},
{"Name": "/project/catch/document/root/more/data", "Value": "deeper"},
{"Name": "/project/catch/document/root", "Value": "the_road_is_long"},
{"Name": "/project/catch/name-with-dashes", "Value": "them-dashes"}
]}
''';
const rawJsonRootTree = '''
{"Parameters": [
{"Name": "/lost/in/translation", "Value": "do-not-catch-me"},
{"Name": "/project/catch/sname", "Value": "expect-me"},
{"Name": "/project/catch/document/root/data", "Value": "deep"},
{"Name": "/project/catch/document/root/more/data", "Value": "deeper"},
{"Name": "/project/catch/name-with-dashes", "Value": "them-dashes"}
]}
''';
const rawJsonDeepDict = '''
{"Parameters": [
{"Name": "/project/catch/root/key1", "Value": "value1"},
{"Name": "/project/catch/root/key2", "Value": "value2"},
{"Name": "/project/catch/root/branch1/key3", "Value": "value3"},
{"Name": "/project/catch/root/branch2/key4", "Value": "value4"},
{"Name": "/project/catch/root/branch2/subbranch1/key5", "Value": "value5"},
{"Name": "/project/catch/root/branch2/subbranch1/key6", "Value": "value6"}
]}
''';
const apacheConfiguration = '''<VirtualHost *:80>
    ServerName expect-me
    DocumentRoot the_road_is_long
</VirtualHost>
''';
const apacheConfigurationSafe = '''<VirtualHost *:80>
    ServerName expect-me
    DocumentRoot the_road_is_long
    Rusty them-dashes
</VirtualHost>
''';

const expectedIniOutput = '''
[document]
root=the_road_is_long

[name-with-dashes]
validation=them-dashes

[sname]
named=expect-me

''';

const globalResult =
    '{\'document\': {\'root\': \'the_road_is_long\'}, \'name-with-dashes\': \'them-dashes\', \'sname\': \'expect-me\'}\n';

const expectedSplitOutput = '''
the
road
is
long
''';

void main() {
  test('test get render context simple', () {
    var data = jsonDecode(rawJson);
    var env = getContext(data, prefix);
    expect(env, {
      'sname': 'expect-me',
      'document': {
        'root': 'the_road_is_long',
      },
      'name-with-dashes': 'them-dashes',
    });
  });

  test('test get render context root value', () {
    var data = jsonDecode(rawJsonRootValue);
    var env = getContext(data, prefix, showWarning: false);
    expect(env, {
      'sname': 'expect-me',
      'document': {
        'root': 'the_road_is_long',
      },
      'name-with-dashes': 'them-dashes',
    });
  });

  test('test get render context simple with safe mode', () {
    var data = jsonDecode(rawJson);
    var env = getContext(data, prefix, safeMode: true);
    expect(env, {
      'sname': 'expect-me',
      'document': {
        'root': 'the_road_is_long',
      },
      'name_with_dashes': 'them-dashes',
    });
  });

  test('test get render context root value with safe mode', () {
    var data = jsonDecode(rawJsonRootValue);
    var env = getContext(data, prefix, safeMode: true, showWarning: false);
    expect(env, {
      'sname': 'expect-me',
      'document': {
        'root': 'the_road_is_long',
      },
      'name_with_dashes': 'them-dashes',
    });
  });

  test('test get render context root tree', () {
    var data = jsonDecode(rawJsonRootTree);
    var env = getContext(data, prefix);
    expect(env, {
      'sname': 'expect-me',
      'document': {
        'root': {
          'data': 'deep',
          'more': {
            'data': 'deeper',
          }
        }
      },
      'name-with-dashes': 'them-dashes',
    });
  });

  test('test get render context deep dict', () {
    var data = jsonDecode(rawJsonDeepDict);
    var env = getContext(data, prefix);
    expect(env, {
      'root': {
        'key1': 'value1',
        'key2': 'value2',
        'branch1': {
          'key3': 'value3',
        },
        'branch2': {
          'key4': 'value4',
          'subbranch1': {
            'key5': 'value5',
            'key6': 'value6',
          }
        }
      },
    });
  });

  test('test get render context does not depend on prefix trailing slash', () {
    final prefix = '/project/catch';
    var data = jsonDecode(rawJson);
    var env1 = getContext(data, prefix);
    var env2 = getContext(data, prefix + '/');
    expect(
      env1,
      env2,
    );
  });

  test('test get templates environment', () {
    const path = '.';
    var jenv = getJinjaEnvironment(path);
    var template = jenv.fromString('[# comment #][[ test|upper ]]');
    expect(template.renderMap({'test': 'the_result'}), 'THE_RESULT');
  });

  test('test read json', () {
    final rawJson = File('test/data.json').readAsStringSync();
    final data = jsonDecode(rawJson);

    expect(readJsonData('test/data.json'), data);
  });

  test('test read json file error', () {
    expect(
        () => readJsonData('test/void.json'),
        throwsA(isA<ReadJsonDataError>().having(
            (ReadJsonDataError error) => error.message,
            'message',
            contains('Cannot open file'))));
  });

  test('test read json malformed json', () {
    expect(
        () => readJsonData('test/bad.json'),
        throwsA(isA<ReadJsonDataError>().having(
            (ReadJsonDataError error) => error.message,
            'message',
            contains('Unexpected character'))));
  });

  test('test render apache config', () {
    final env = getJinjaEnvironment('test');
    final context = getContext(jsonDecode(rawJson), prefix);
    expect(render(env, 'apache_template.conf', context), apacheConfiguration);
  });

  test('test render __global__', () {
    final env = getJinjaEnvironment('test');
    final context = getContext(jsonDecode(rawJson), prefix);
    expect(render(env, 'global.template', context), globalResult);
  });

  test('test render ini config', () {
    final jsonIni = readJsonData('test/ini.json');
    final env = getJinjaEnvironment('test');
    final context = getContext(jsonIni, prefix);
    expect(render(env, 'ini.template', context), expectedIniOutput);
  });

  test('test render splitAndTrim function', () {
    final jsonSplit = readJsonData('test/split.json');
    final env = getJinjaEnvironment('test');
    final context = getContext(jsonSplit, prefix);
    expect(render(env, 'split.template', context), expectedSplitOutput);
  });
}

// vim: set ts=2 sw=2 sts=2 et :
