import 'dart:io';
import 'package:test/test.dart';
import 'package:path/path.dart';
import '../bin/repasto.dart';
import '../bin/repasto.dart' hide main;
import '../bin/repasto.dart' as repasto;
import 'environment_test.dart'
    show apacheConfiguration, apacheConfigurationSafe, prefix;

const expectedHelp = '''repasto 0.5.2
usage:
    repasto -j ENV -p PREFIX -T TEMPLATES_DIR -t TEMPLATE_NAME -o OUTPUT

options and flags:
    -d, --dry-run          dry run mode
    -j, --json-env         path to JSON env file
    -o, --output           output file name
    -p, --prefix           filter prefix for entries in JSON env file
    -T, --templates-dir    path to templates
    -t, --template-name    template name
    -s, --safe-mode        safe mode (change "-" into "_" in context keys)
    -h, --help             this help message

repasto is a tool for rendering data from AWS Parameter Store''';

const expectDebugInfo = '''
### runtime data ###
output              : /path/to/generated/file
prefix              : /in/the/sky/
templates source    : /meet/me/there/
template            : tampleete
json env file       : psdata.json
''';

const expectDebugInfoNoOutput = '''
### runtime data ###
output              : (undefined)
prefix              : /in/the/sky/
templates source    : /meet/me/there/
template            : tampleete
json env file       : psdata.json
''';

class OptionValidation {
  String arg;
  List<String> validationArgs;

  OptionValidation(this.arg, this.validationArgs);
}

void main() {
  test('parser output', () {
    var parser = getParser();

    expect(parser.options.length, 8);
    expect(parser.options['dry-run']?.abbr, 'd');
    expect(parser.options['json-env']?.abbr, 'j');
    expect(parser.options['output']?.abbr, 'o');
    expect(parser.options['prefix']?.abbr, 'p');
    expect(parser.options['templates-dir']?.abbr, 'T');
    expect(parser.options['template-name']?.abbr, 't');
    expect(parser.options['safe-mode']?.abbr, 's');
    expect(parser.options['help']?.abbr, 'h');

    var options =
        parser.parse(['-d', '-t', 'file.jinja2', '--templates-dir=folder']);
    expect(options['dry-run'], true);
    expect(options['template-name'], 'file.jinja2');
    expect(options['templates-dir'], 'folder');
  });

  test('test validate options without dry run', () {
    final checkList = [
      OptionValidation('json-env', <String>[]),
      OptionValidation('prefix', [
        '--json-env',
        'psdata.json',
      ]),
      OptionValidation('templates-dir', [
        '--json-env',
        'psdata.json',
        '--prefix',
        '/in/the/sky/',
      ]),
      OptionValidation('template-name', [
        '--json-env',
        'psdata.json',
        '--prefix',
        '/in/the/sky/',
        '--templates-dir',
        '/meet/me/there/',
      ]),
      OptionValidation('output', [
        '--json-env',
        'psdata.json',
        '--prefix',
        '/in/the/sky/',
        '--templates-dir',
        '/meet/me/there/',
        '--template-name',
        'tampleete',
      ]),
    ];

    checkList.forEach((ov) {
      final arg = ov.arg;
      expect(
          () => validateOptions(getParser().parse(ov.validationArgs)),
          throwsA(isA<MissingOptionError>().having(
              (MissingOptionError error) => error.message,
              'message',
              contains('argument $arg is mandatory'))));
    });
  });

  test('test validate options with dry run', () {
    // output is optional in dry-run mode
    final optionsWithDryRun = OptionValidation('dry-run', [
      '--json-env',
      'psdata.json',
      '--prefix',
      '/in/the/sky/',
      '--templates-dir',
      '/meet/me/there/',
      '--template-name',
      'tampleete',
      '--dry-run'
    ]);

    expect(validateOptions(getParser().parse(optionsWithDryRun.validationArgs)),
        true);
  });

  test('test help', () {
    final parser = getParser();
    expect(generateHelp(parser), expectedHelp);
  });

  test('test runtime debug', () {
    final optionsWithDryRun = OptionValidation('dry-run', [
      '--json-env',
      'psdata.json',
      '--prefix',
      '/in/the/sky/',
      '--templates-dir',
      '/meet/me/there/',
      '--template-name',
      'tampleete',
      '--output',
      '/path/to/generated/file',
      '--dry-run'
    ]);

    expect(
        getRunTimeDebugInfo(
            getParser().parse(optionsWithDryRun.validationArgs)),
        expectDebugInfo);
  });

  test('test runtime debug without output', () {
    final optionsWithDryRun = OptionValidation('dry-run', [
      '--json-env',
      'psdata.json',
      '--prefix',
      '/in/the/sky/',
      '--templates-dir',
      '/meet/me/there/',
      '--template-name',
      'tampleete',
      '--dry-run'
    ]);

    expect(
        getRunTimeDebugInfo(
            getParser().parse(optionsWithDryRun.validationArgs)),
        expectDebugInfoNoOutput);
  });

  test('test main', () {
    void actualTest(temp) {
      final output = join(temp.path, 'apache.conf');
      final args = [
        '-j',
        'test/data.json',
        '-p',
        prefix,
        '-T',
        'test',
        '-t',
        'apache_template.conf',
        '-o',
        output
      ];
      repasto.main(args);
      expect(File(output).readAsStringSync(), apacheConfiguration);
    }

    final temp = Directory.systemTemp.createTempSync();
    try {
      actualTest(temp);
    } finally {
      temp.deleteSync(recursive: true);
    }
  });

  test('test main safe mode', () {
    void actualTest(temp) {
      final output = join(temp.path, 'apache.conf');
      final args = [
        '-j',
        'test/data.json',
        '-p',
        prefix,
        '-T',
        'test',
        '-t',
        'apache_template_safe.conf',
        '-o',
        output,
        '-s'
      ];
      repasto.main(args);
      expect(File(output).readAsStringSync(), apacheConfigurationSafe);
    }

    final temp = Directory.systemTemp.createTempSync();
    try {
      actualTest(temp);
    } finally {
      temp.deleteSync(recursive: true);
    }
  });
}
// vim: set ts=2 sw=2 sts=2 et :
