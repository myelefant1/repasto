PROJECT?=repasto
DART_CMD?=dart
PUB_CMD?=$(DART_CMD) pub
PUB_VARS?=--concurrency=4 --reporter=expanded

all: install-deps tests build

install-deps:
	$(PUB_CMD) get

outdated-deps:
	$(PUB_CMD) outdated

upgrade-deps:
	$(PUB_CMD) upgrade

tests:
	$(DART_CMD) run test $(PUB_VARS)

build: install-deps
	@mkdir -p build
	$(DART_CMD) compile exe bin/$(PROJECT).dart -o build/$(PROJECT)

clean:
	rm -rf build
