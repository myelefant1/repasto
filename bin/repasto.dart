import 'dart:convert';
import 'dart:collection';
import 'dart:io';
import 'package:args/args.dart';
import 'package:jinja/jinja.dart';
import 'version.dart';

const templateExtensions = {'ini', 'conf', 'tmpl', 'template', 'jj2', 'jinja2'};
const spacer = '    ';

ArgParser getParser() {
  final parser = ArgParser();
  parser
    ..addFlag('dry-run', abbr: 'd', help: 'dry run mode', negatable: false)
    ..addOption('json-env', abbr: 'j', help: 'path to JSON env file')
    ..addOption('output', abbr: 'o', help: 'output file name')
    ..addOption('prefix',
        abbr: 'p', help: 'filter prefix for entries in JSON env file')
    ..addOption('templates-dir', abbr: 'T', help: 'path to templates')
    ..addOption('template-name', abbr: 't', help: 'template name')
    ..addFlag('safe-mode',
        abbr: 's',
        help: 'safe mode (change "-" into "_" in context keys)',
        negatable: false)
    ..addFlag('help', abbr: 'h', help: 'this help message', negatable: false);
  return parser;
}

Map<String, Object?> getContext(Map<dynamic, dynamic> data, String prefix,
    {bool safeMode = false, bool showWarning = true}) {
  /*
  We use a SplayTreeMap to order entries according to key names in order to
  bring consistency of processing, regardless of entries order in data
  generated by Parameter Store
  */
  var basicEnv = SplayTreeMap();

  // make sure prefix ends with /
  if (!prefix.endsWith('/')) {
    prefix += '/';
  }

  data['Parameters'].forEach((parameter) {
    var parameterName = parameter['Name'];
    if (parameterName.toString().startsWith(prefix)) {
      // calling substring is maybe faster|better
      var key = parameterName.replaceFirst(prefix, '');
      basicEnv[key] = parameter['Value'];
    }
  });

  var env = <String, Object?>{};
  var partEnv = env;

  basicEnv.forEach((name, value) {
    var path = [];
    var finalPart;
    name.split('/').forEach((part) {
      if (safeMode) {
        finalPart = part.replaceAll(RegExp(r'-'), '_');
      } else {
        finalPart = part;
      }
      path.add(finalPart);
    });
    var leaf = path.last;

    for (var part in path) {
      if (part == leaf) {
        partEnv[leaf] = value;
      } else {
        partEnv.putIfAbsent(part, () => <String, Object?>{});

        if (partEnv[part] is Map<String, Object?>) {
          partEnv = partEnv[part] as Map<String, Object?>;
        } else {
          if (showWarning) {
            stderr.writeln('Warning: try to update existing entry!!!');
            stderr.writeln('conflict at $part from $name');
          }
          break;
        }
      }
    }
    partEnv = env;
  });
  return env;
}

List<String> splitAndTrim(Map<Object?, Object?> args) {
  final value = args['value'].toString();
  final separator = args['separator'].toString();
  return value.split(separator).map((v) {
    return v.trim();
  }).toList();
}

Environment getJinjaEnvironment(String path) {
  final loader = FileSystemLoader(
      path: path, followLinks: true, extensions: templateExtensions);
  return Environment(
      loader: loader,
      blockEnd: '%]',
      blockStart: '[%',
      commentEnd: '#]',
      commentStart: '[#',
      variableEnd: ']]',
      variableStart: '[[',
      globals: {'split_and_trim': splitAndTrim});
}

String render(
    Environment jenv, String template_name, Map<String, Object?> context) {
  final template = jenv.getTemplate(template_name);
  final globalContext = <String, Object?>{'__global__': context};
  final finalContext = {
    ...globalContext,
    ...context,
  };
  return template.renderMap(finalContext);
}

class ReadJsonDataError implements Exception {
  final String? message;

  ReadJsonDataError(this.message);

  @override
  String toString() => message ?? 'ReadJsonDataError';
}

class MissingOptionError implements Exception {
  final String? message;

  MissingOptionError(this.message);

  @override
  String toString() => message ?? 'MissingOptionError';
}

Map<String, Object?> readJsonData(String path) {
  try {
    return jsonDecode(File(path).readAsStringSync());
  } on FileSystemException catch (err) {
    throw ReadJsonDataError(err.message);
  } on FormatException catch (err) {
    throw ReadJsonDataError(err.message);
  }
}

bool validateOptions(ArgResults args) {
  var optionsToCheck = [
    'json-env',
    'prefix',
    'templates-dir',
    'template-name',
  ];
  if (!args['dry-run']) {
    optionsToCheck.add('output');
  }

  optionsToCheck.forEach((arg) {
    if (!args.options.contains(arg)) {
      throw MissingOptionError('argument $arg is mandatory');
    }
  });
  return true;
}

String generateHelp(parser) {
  var sb = StringBuffer('repasto $version\n');
  sb.writeln('usage:');
  sb.writeln(
      '${spacer}repasto -j ENV -p PREFIX -T TEMPLATES_DIR -t TEMPLATE_NAME -o OUTPUT');
  sb.writeln('\noptions and flags:');
  parser.usage.split('\n').forEach((row) {
    sb.write('$spacer$row\n');
  });
  sb.writeln('\nrepasto is a tool for rendering data from AWS Parameter Store');
  return sb.toString().trimRight();
}

String getRunTimeDebugInfo(ArgResults args) {
  var sb = StringBuffer('### runtime data ###\n');
  var output = args['output'] ?? '(undefined)';
  sb.writeln('output              : $output');
  sb.writeln('prefix              : ${args['prefix']}');
  sb.writeln('templates source    : ${args['templates-dir']}');
  sb.writeln('template            : ${args['template-name']}');
  sb.writeln('json env file       : ${args['json-env']}');
  return sb.toString();
}

void main(List<String> arguments) {
  final parser = getParser();
  var argResults = parser.parse(arguments);

  if (argResults['help']) {
    print(generateHelp(parser));
  } else {
    try {
      validateOptions(argResults);
      var context = getContext(
          readJsonData(argResults['json-env']), argResults['prefix'],
          safeMode: argResults['safe-mode']);
      var jenv = getJinjaEnvironment(argResults['templates-dir']);

      final result = render(jenv, argResults['template-name'], context);

      if (argResults['dry-run']) {
        print(getRunTimeDebugInfo(argResults));
        print(result);
      } else {
        var output = File(argResults['output']);
        output.writeAsStringSync(result);
      }
    } on Exception catch (error) {
      print(error);
      if (error is MissingOptionError) {
        print('Run "repasto --help" for all options');
      }
    }
  }
}

// vim: set ts=2 sw=2 sts=2 et :
