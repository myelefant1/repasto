repasto 0.5.2
===


REnder PArameter STOre data files to arbitrary files using Jinja2-like templates. :spaghetti:

### Miscellaneous

:warning: Template rendering is done with a dart jinja2-almost-compatible library [jinja-dart](https://pub.dev/documentation/jinja/latest/). Check out documentation for limitations and differences vs [Python jinja2](https://jinja.palletsprojects.com/en/3.0.x/) implementation.

:warning: Templates use Jinja syntax but characters `{` and `}`are respectively replaced by `[` and `]`. Thus, rendering files usable as ansible templates is possible.

:warning: Data on the Parameter Store exported JSON file are modified by `repasto` before being rendered. repasto turn the flat JSON into a map. Entries are sorted by field `Name` before processing (see below [Path to keys transformation](#path-to-keys-transformations)).

Operations on values of key `Parameters` while iterating:
- only entries (field `Name`) starting with the prefix specified by argument `prefix` are kept, others are ignored,
- optionally, characters `-` in entries (field `Name`) are replaced with `_` (argument `--safe-mode`).

Data in field `Value` are left untouched.

### Path to keys transformation

Parameter Store keys are described as path (`/` separated); while iterating of entries, we may have keys overriding.
Let's consider this JSON document:
```json
{
  "Parameters": [
    {"Name": "/root/branch", "Value":"a value"},
    {"Name": "/root/branch/leaf", "Value":"another value"}
  ]
}
```
repasto will transform this structure into following dictionary (with prefix `/`; using prefix `root` would yield another result).
```python=
{
    "root": {
        "branch": "a value",
        # the entry `/root/branch/leaf` cannot be added to the tree,
        # because it would override scalar value of `root/branch`.
        #"branch": {
        #   "leaf": "another value"
        #}
    }
}
```
In a general matter, repasto will always ignore any entries that would override an existing scalar value. All ignored entries are displayed as warnings.
The previous example would show this warning:
```
Warning: try to update existing entry!!!
conflict at branch from root/branch/leaf
```

### Usage
```
$ repasto --help
repasto 0.5.2
usage:
    repasto -j ENV -p PREFIX -T TEMPLATES_DIR -t TEMPLATE_NAME -o OUTPUT

options and flags:
    -d, --dry-run          dry run mode
    -j, --json-env         path to JSON env file
    -o, --output           output file name
    -p, --prefix           filter prefix for entries in JSON env file
    -T, --templates-dir    path to templates
    -t, --template-name    template name
    -s, --safe-mode        safe mode (change "-" into "_" in context keys)
    -h, --help             this help message

repasto is a tool for rendering data from AWS Parameter Store
```


### Extra functions

In addition to builtin filters and functions, repasto provides a function able to split a string, and trim each resulting parts. The function is named `split_and_trim`. It accepts a dict a parameter, which must contains two values: `value` and `separator`.

```
[%- for elem in split_and_trim({'value': ' elem1 , elem2 , elem3  ', 'separator': ','}) -%]
row[[ loop.index0 ]] = [[ elem ]]
[% endfor -%]
```

will render

```
row0 = elem1
row1 = elem2
row2 = elem3
```

### Examples

Forged example of Parameter Store data
```json
{
  "Parameters": [
    { "Name": "/lost/in/translation", "Value": "do-not-catch-me" },
    { "Name": "/project/catch/sname", "Value": "expect-me" },
    { "Name": "/project/catch/document/root", "Value": "the_road_is_long" },
    { "Name": "/project/catch/name-with-dashes", "Value": "them-dashes" }
  ]
}
```

Examples of template:

```htmlmixed=
<VirtualHost *:80>
    ServerName [[ sname ]]
    DocumentRoot [[ document.root ]]
</VirtualHost>
```

:information_source: the template below can only be rendered as expected from sample data, when repasto is ran with `safe-mode` enabled.
```htmlmixed=
<VirtualHost *:80>
    ServerName [[ sname ]]
    DocumentRoot [[ document.root ]]
    Rusty [[ name_with_dashes ]]
</VirtualHost>
```


Examples:
```
$ repasto -j test/data.json -p "/project/catch/" -T test -t apache_template_safe.conf -o test -d

### runtime data ###
output              : test
prefix              : /project/catch/
templates source    : test
template            : apache_template.conf
json env file       : test/data.json

<VirtualHost *:80>
    ServerName expect-me
    DocumentRoot the_road_is_long
    Rusty
</VirtualHost>

$ repasto -j test/data.json -p "/project/catch/" -T test -t apache_template_safe.conf -o test -s -d

### runtime data ###
output              : test
prefix              : /project/catch/
templates source    : test
template            : apache_template.conf
json env file       : test/data.json

<VirtualHost *:80>
    ServerName expect-me
    DocumentRoot the_road_is_long
    Rusty them-dashes
</VirtualHost>

```

:page_facing_up: The files shown in examples are available in `test` folder of code repository.
