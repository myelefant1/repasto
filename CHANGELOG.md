## unreleased
- Build repasto with dart 2.16
- Updated dependencies

## 0.5.2

- Added template function 'split_and_trim'
- Updated dependencies

## 0.5.1

- Build repasto with dart 2.14
- Updated dependencies

## 0.5.0

- Do not perform key transformation by default
- Added parameter '--safe-mode' that enables key transformation

## 0.4.3

- Made sure the same outpout is yielded, regardless of presence of a trailing slash in prefix (/project/config vs /project/config/)
- Updated dependencies

## 0.4.2

- Simple release to validate updated gitlab CI

## 0.4.1

- Misc refacto
- Improved version management
- Updated release process

## 0.4.0

- Transformation of JSON data to a dictionary now respects tree structure brought by path-like entry names.
- Added '__global__' entry to render context, so we can iterate over context dictionary with knowledge of root entries.
- Added test and template example for generation of .ini files.

## 0.2.0

- Initial version.
